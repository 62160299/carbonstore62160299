/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watcharanat62160299.carbonstore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class StoreService {

    private static ArrayList<Store> storeList = new ArrayList<>();

    static {
       load();
    }

    public static Store getStore(int index) {
        return storeList.get(index);

    }

    public static boolean addStore(Store store) {
        storeList.add(store);
        save();
        return true;
    }

    public static boolean delStore(Store store) {
        storeList.remove(store);
        save();
        return true;
    }

    public static boolean delStore(int index) {
        storeList.remove(index);
        save();
        return true;
    }

    public static boolean delAll() {
        storeList.clear();
        save();
        return true;
    }

   

    public static ArrayList<Store> getStore() {
        return storeList;
    }

    public static boolean updateStore(int index, Store store) {
        storeList.set(index, store);
        save();
        return true;
    }

    public static double getSumAmount() {
        double sumAmount = 0;
        for (int i = 0; i < storeList.size(); i++) {
            sumAmount += Double.valueOf((storeList.get(i).getAmount()));
        }
        return sumAmount;
    }

    public static double getSumPrice() {
        double sumPrice = 0;
        for (int i = 0; i < storeList.size(); i++) {
            sumPrice += Double.valueOf((storeList.get(i).getPrice())) * Double.valueOf((storeList.get(i).getAmount()));
        }
        return sumPrice;
    }
    public static void save(){
        File file = null;
            FileOutputStream fos = null;
            ObjectOutputStream oos = null;
        try {
            file = new File("storecarbon.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            Object storelis;
            oos.writeObject(storeList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StoreService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StoreService.class.getName()).log(Level.SEVERE, null, ex);
        }
       
     }
     public static void load(){
            File file = null;
            FileInputStream fis = null;
            ObjectInputStream ois = null;
        try {
            file = new File("storecarbon.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            storeList = (ArrayList<Store>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StoreService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StoreService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StoreService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
     }
    
    
    

    static void updateStore(Store store) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
